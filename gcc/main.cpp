#include "lib_clang.hpp"
#include "lib_gcc.hpp"

int main() {
  clang::foo();
  gcc::foo();
  return 0;
}
