.PHONY: all clean

all:
	$(MAKE) -C ./lib_clang
	$(MAKE) -C ./lib_gcc
	$(MAKE) -C ./clang
	$(MAKE) -C ./gcc

lib_clang:
	$(MAKE) -C ./lib_clang

lib_gcc:
	$(MAKE) -C ./lib_gcc

clang:
	$(MAKE) -C ./clang

gcc:
	$(MAKE) -C ./gcc

clean:
	$(MAKE) -C ./lib_clang clean
	$(MAKE) -C ./lib_gcc clean
	$(MAKE) -C ./clang clean
	$(MAKE) -C ./gcc clean
